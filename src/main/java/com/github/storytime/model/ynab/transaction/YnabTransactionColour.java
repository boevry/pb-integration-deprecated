package com.github.storytime.model.ynab.transaction;
@Deprecated
public interface YnabTransactionColour {
    String RED = "Red";
    String GREEN = "Green";
    String BLUE = "Blue";
}
