ALTER TABLE ynab_sync_config
    ADD COLUMN enabled boolean DEFAULT false;