package com.github.storytime.model.db.inner;

/**
 * Values is saved in db by name, so names changed not allowed
 */
public enum AdditionalComment {
    NBU_PREV_MOUTH_LAST_BUSINESS_DAY,
    PB_CURRENT_BUSINESS_DAY
}
