package com.github.storytime.model.ynab;

import com.github.storytime.model.db.YnabSyncConfig;
import com.github.storytime.model.zen.ZenResponse;

import java.util.Optional;
@Deprecated
public class YnabToZenSyncHolder {
    private Optional<ZenResponse> zenResponse;
    private YnabSyncConfig ynabSyncConfig;

    public YnabToZenSyncHolder(final Optional<ZenResponse> zenResponse,
                               final YnabSyncConfig ynabSyncConfig) {
        this.zenResponse = zenResponse;
        this.ynabSyncConfig = ynabSyncConfig;
    }

    public Optional<ZenResponse> getZenResponse() {
        return zenResponse;
    }

    public YnabToZenSyncHolder setZenResponse(Optional<ZenResponse> zenResponse) {
        this.zenResponse = zenResponse;
        return this;
    }

    public YnabSyncConfig getYnabSyncConfig() {
        return ynabSyncConfig;
    }

    public YnabToZenSyncHolder setYnabSyncConfig(YnabSyncConfig ynabSyncConfig) {
        this.ynabSyncConfig = ynabSyncConfig;
        return this;
    }
}
