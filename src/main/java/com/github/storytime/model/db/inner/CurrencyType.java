package com.github.storytime.model.db.inner;

/**
 * Values is saved in db by name, so names changed not allowed
 */
public enum CurrencyType {
    USD,
    EUR
}
